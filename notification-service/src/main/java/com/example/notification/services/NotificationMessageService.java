package com.example.notification.services;

import com.example.notification.entities.NotificationMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Notification Service - Provides the action to process the message.
 */
@Service
public class NotificationMessageService {

    @Autowired
    private EmailDispatcher emailDispatcher;

    public boolean dispatch(NotificationMessage notificationMessage) {

        return emailDispatcher.send(notificationMessage);

    }

}
