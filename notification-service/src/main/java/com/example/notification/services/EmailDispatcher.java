package com.example.notification.services;

import com.example.notification.entities.NotificationMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailPreparationException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpHeaders.FROM;

/**
 * Email dispatcher - dispaches the received event message via email
 */
@Service
@Slf4j
public class EmailDispatcher {

    @Autowired
    private JavaMailSender emailSender;

    public boolean send(NotificationMessage notificationMessage) {

        try {

            MimeMessagePreparator preparator = (mimeMessage) -> {

                List<String> destinations = notificationMessage.getDestinations();

                String addressList = "";

                if(destinations.size() > 1) {

                    addressList = toStringList(destinations);

                } else {
                    log.error("Destination not found!");
                    return;
                }

                InternetAddress[] internetAddresses = InternetAddress.parse(addressList);

                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setTo(internetAddresses);
                messageHelper.setSubject(notificationMessage.getSubject());
                messageHelper.setText(notificationMessage.getMessage(), false);
                messageHelper.setFrom(FROM, "Sistema UNESP");

            };

            emailSender.send(preparator);

            log.debug("Notification dispatched by E-MAIL!");

            return true;

        } catch (MailException exception) {

            log.debug("Erro to send notification message via E-MAIL!", exception);

            return false;
        }

    }

    private String toStringList(Collection<String> list) {

        String strList = "";

        for (String dest : list) {
            if(!strList.isEmpty()) {
                strList = strList.concat(",").concat(dest);
            } else {
                strList = strList.concat(dest);
            }
        }

        return strList;
    }

}
