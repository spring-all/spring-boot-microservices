package com.example.notification.config;

import com.example.notification.entities.NotificationMessage;
import com.example.notification.services.NotificationMessageService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * RabbitMQ message listener implementation
 */
@Component
@Slf4j
public class NotificationMessageListener implements MessageListener {

    @Autowired
    private NotificationMessageService notificationService;

    private ObjectMapper mapper = new ObjectMapper();

    public NotificationMessageListener() {
        mapper.registerModule(new JavaTimeModule());
        mapper.enable(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES);

    }

    /**
     * Method executed when a new event is received by the listener
     * @param message The event message received
     */
    /* (non-Javadoc)
     * @see org.springframework.amqp.core.MessageListener#onMessage(org.springframework.amqp.core.Message)
     */
    @Override
    public void onMessage(Message message) {

        byte[] body = message.getBody();

        log.debug("Captured notification: " + new String(body));

        try {
            NotificationMessage notificationMessage = mapper.readValue(body, NotificationMessage.class);

            boolean dispatched = this.notificationService.dispatch(notificationMessage);

            log.info("Dispatched: " + dispatched);

        } catch (IOException e) {
            log.error("Error to deserialize notification message: " + new String(body), e);
        }

    }

}

