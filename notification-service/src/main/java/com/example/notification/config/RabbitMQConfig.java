package com.example.notification.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * RabbitMQ Configuration class
 *
 * Declaration of exchanges and queues to connect and listen or send events.
 */
@Configuration
public class RabbitMQConfig {

    @Value("${app.rabbitmq.exchanges.notify}")
    private String notifyExchange;

    @Bean(name="notifyQueue")
    Queue notifyQueue() {
        //Queue empty name - The broker decides the name
        return new Queue("", true, true, true);
    }

    @Bean
    FanoutExchange notifyExchange() {
        return new FanoutExchange(notifyExchange, true, false);
    }

    @Bean
    Binding notifyBinding() {
        return BindingBuilder.bind(notifyQueue()).to(notifyExchange());
    }

    @Autowired
    private NotificationMessageListener messageListener;

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        container.addQueues(notifyQueue());
        container.setMessageListener(messageListener);

        return container;
    }

}
