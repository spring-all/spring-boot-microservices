package com.example.notification.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Notification message POJO
 */
@Getter
@Setter
public class NotificationMessage {

    private String subject;
    private String message;
    private List<String> destinations;

}
