package com.example.projects.entities.entity;

public interface ProjectView {

    Integer getId();
    String getTitle();
}
