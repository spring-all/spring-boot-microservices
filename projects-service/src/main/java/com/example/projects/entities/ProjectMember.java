package com.example.projects.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
@IdClass(ProjectMemberId.class)
@Table(name="projects_members")

public class ProjectMember {

    @Id
    @MapsId("id")
    @ManyToOne(optional=false)
    @JoinColumn(name="project_id", referencedColumnName="id", foreignKey = @ForeignKey(name = "fk_projects"))
    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId = true)
    private Project project;

    @Id
    private Integer member;

}

