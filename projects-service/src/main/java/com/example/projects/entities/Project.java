package com.example.projects.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Project class
 *
 * @see {@link @GeneratedValue} and {@link SequenceGenerator} for {@link Id} generatation options
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Project {

    /**
     * PRIMARY KEY - auto increment controlled by sequence
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="title", nullable = false) //Database NOT NULL
    @NonNull //Java not null - if null, throws NPE
    private String title;

    @Column(name="description", nullable = false)
    @NonNull
    private String description;

    @Column(name="start_date", nullable = false)
    @NonNull
    private Date startDate;

    @Column(name="end_date")
    private Date endDate;

    @Column(name="opened")
    private boolean opened;

    @OneToMany(mappedBy = "project", targetEntity = ProjectMember.class, cascade={CascadeType.PERSIST, CascadeType.REMOVE})
    @OnDelete(action= OnDeleteAction.CASCADE)
    private Set<ProjectMember> members;

}
