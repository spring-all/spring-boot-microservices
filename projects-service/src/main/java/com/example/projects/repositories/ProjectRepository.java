package com.example.projects.repositories;

import com.example.projects.entities.Project;
import com.example.projects.entities.entity.ProjectView;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProjectRepository extends PagingAndSortingRepository<Project, Integer> {

    List<ProjectView> findAllByMembers_Member(Integer memberId);

}
