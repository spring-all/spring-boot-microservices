package com.example.projects.controllers;

import com.example.projects.controllers.exceptions.BadRequestException;
import com.example.projects.entities.Project;
import com.example.projects.entities.entity.ProjectView;
import com.example.projects.services.ProjectService;
import com.example.projects.utils.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Project REST endpoint controllers
 */
@RestController
@RequestMapping(value="/projects")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @GetMapping
    public Page<Project> list(Pageable pageable) {
        return this.projectService.list(pageable);
    }

    @GetMapping(value = "/by-members")
    public ApiResponse<?> listMemberProjects(@RequestParam(name="memberId") Integer memberId) {
        List<ProjectView> memberProjects = this.projectService.getMemberProjects(memberId);

        return ApiResponse.builder().success(true).data(memberProjects).build();
    }

    @GetMapping(value="/{id}")
    public Project get(@PathVariable(name="id") Integer id) {
        return this.projectService.get(id);
    }

    @PostMapping
    public Project create(@RequestBody Project person){
        return this.projectService.register(person);
    }

    @PutMapping(value="/{id}")
    public Project update(@PathVariable(name="id") Integer id, @RequestBody Project person) {

        if(!id.equals(person.getId())) {
            throw new BadRequestException("Project id and path id mismatch!");
        }

        return this.projectService.update(person);
    }

    @DeleteMapping(value="/{id}")
    public ApiResponse<?> delete(@PathVariable(name="id") Integer id) {

        this.projectService.remove(id);

        return ApiResponse.builder().success(true).data("Project removed!").build();
    }

}