package com.example.projects.services;

import com.example.projects.entities.Project;
import com.example.projects.entities.ProjectMember;
import com.example.projects.entities.entity.ProjectView;
import com.example.projects.repositories.ProjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * Project Service
 */
@Service
@Slf4j
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    public Project register(Project p) {

        log.info("Registering Project: {}", p.getTitle());

        /*
        Allows simultaneous registration of project and members.
         */
        for (ProjectMember member : p.getMembers()) {
            member.setProject(p);
        }

        return this.projectRepository.save(p);

    };

    public Project update(Project p) throws EntityNotFoundException {

        if(this.projectRepository.existsById(p.getId())){

            log.info("Updating Project " + p.getId());
            return this.projectRepository.save(p);

        }else{

            String msg = "Project with id[" + p.getId() + "] not found to be updated!";
            log.error(msg);
            throw new EntityNotFoundException(msg);

        }

    };

    public Project get(Integer id) {

        return this.projectRepository.findById(id).orElse(null);

    };

    public Page<Project> list(Pageable pageable) {

        return this.projectRepository.findAll(pageable);

    };

    public void remove(Project p) throws EntityNotFoundException {

        this.remove(p.getId());

    };

    public void remove(Integer id) throws EntityNotFoundException {

        if(this.projectRepository.existsById(id)){

            log.info("Removing Project {}", id);
            this.projectRepository.deleteById(id);

        }else{

            String msg = "Project with id[" + id + "] not found to be removed!";
            log.error(msg);
            throw new EntityNotFoundException(msg);

        }

    };

    public List<ProjectView> getMemberProjects(Integer memberId) {

        return this.projectRepository.findAllByMembers_Member(memberId);

    }

}
