package com.example.projects.services;

import com.example.projects.utils.NotificationMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.MimeTypeUtils;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class NotificationService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${spring.application.name}")
    private String appName;

    @Value("${app.rabbitmq.exchanges.notify}")
    private String notifyExchange;

    public void notify(List<String> destinations, String subject, String message) {

        NotificationMessage notificationMessage = new NotificationMessage();
        notificationMessage.setDestinations(destinations);
        notificationMessage.setSubject(subject);
        notificationMessage.setMessage(message);

        this.rabbitTemplate.convertAndSend(notifyExchange, "", notificationMessage, m-> {
            m.getMessageProperties().getHeaders().put("origin", appName);
            m.getMessageProperties().setContentType(MimeTypeUtils.APPLICATION_JSON_VALUE);
            m.getMessageProperties().setContentEncoding(StandardCharsets.UTF_8.name());
            return m;
        }, new CorrelationData("notification-message"));

    }
}
