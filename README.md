# Spring Boot Microservices

Example project to show main Spring Boot modules used to build a microservices architecture.

## Environment and Tools

Information about the tools and environment used in the example

### IntelliJ

[Download](https://www.jetbrains.com/idea/download/) and install IntelliJ for your OS.

For linux, just download, extract the tar.gz package and run `ideia.sh`.

### Docker CE

Follow docker installation instructions for your OS:

Windows: https://hub.docker.com/editions/community/docker-ce-desktop-windows

Ubuntu: https://docs.docker.com/install/linux/docker-ce/ubuntu/

**Important:** Don't forget to install [Docker Compose](https://docs.docker.com/compose/install/) as well.

### PostgreSQL & PgAdmin

Using the [`docker-compose.yml`](docker-compose.yml):

	$ docker-compose up -d postgresql pgadmin

Both PgAdmin and PostgreSQL images will be pulled from Docker Hub if not exist and both containers will start. 

A link will be stablished between the containers. 

Access `http://localhost:5555` to se pgAdmin.

	Username: user@domain.com
	Password: pguser

Right click on 'Servers' -> 'Create' -> 'Server...'

Give a name to the server and set the following connections parameters:

	Host name / address: postgresql
	Port: 5432
	Maintenance Database: postgres
	Username: postgres
	Password: postgres


**NOTE:** Using the default setup defined in the docker-compose file, if the postgresql container is deleted, all databases will be lost. For more configurations options see: https://hub.docker.com/_/postgres

### RabbitMQ

Using the [`docker-compose.yml`](docker-compose.yml):

	$ docker-compose up -d rabbit

**NOTE:** To download all images and execute all containers, run `docker-compose up -d`