package com.example.people.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;

/**
 * Person class
 *
 * @see {@link @GeneratedValue} and {@link SequenceGenerator} for {@link Id} generatation options
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Person {

    /**
     * PRIMARY KEY - auto increment controlled by sequence
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="name", nullable = false) //Database NOT NULL
    @NonNull //Java not null - if null, throws NPE
    private String name;

    @Column(name="email", nullable = false, unique = true)
    @NonNull
    private String email;

    @Column(name="organization", nullable = false)
    @NonNull
    private String organization;

}