package com.example.people.services.remote;

import com.example.people.utils.ApiResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="projects-service")
public interface RemoteProjectService {

    /**
     * GET request to /projects?memberId=id
     * @param id Project member identifier
     * @return List of projects the person is member of
     */
    @GetMapping("/projects/by-members")
    ApiResponse<?> getPersonProjects(@RequestParam(name="memberId") Integer id);

}
