package com.example.people.services;

import com.example.people.entities.Person;
import com.example.people.repositories.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.Optional;

/**
 * Person Service
 */
@Service
@Slf4j
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private NotificationService notificationService;

    public Person register(Person p) {

        log.info("Registering person: {}", p);
        Person savedPerson = this.personRepository.save(p);

        this.notificationService.notify(Arrays.asList(savedPerson.getEmail()), "Cadastro Realizado",
                "Cadastro realizado com sucesso no Spring Boot Microservice!");

        return savedPerson;

    };

    public Person update(Person p) throws EntityNotFoundException {

        if(this.personRepository.existsById(p.getId())){

            log.info("Updating person " + p.getId());
            Person updatedPerson =  this.personRepository.save(p);

            this.notificationService.notify(Arrays.asList(updatedPerson.getEmail()), "Cadastro Atualizado",
                    "Seu cadastro foi atualizado no Spring Boot Microservice!");

            return updatedPerson;

        }else{

            String msg = "Person with id[" + p.getId() + "] not found to be updated!";
            log.error(msg);
            throw new EntityNotFoundException(msg);

        }

    };

    public Person get(Integer id) {

        return this.personRepository.findById(id).orElse(null);

    };

    public Page<Person> list(Pageable pageable) {

        return this.personRepository.findAll(pageable);

    };

    public void remove(Person p) throws EntityNotFoundException {

        this.remove(p.getId());

    };

    public void remove(Integer id) throws EntityNotFoundException {

        if(this.personRepository.existsById(id)){

            Optional<Person> optionalPerson = this.personRepository.findById(id);

            Person person = optionalPerson.get();

            this.notificationService.notify(Arrays.asList(person.getEmail()), "Cadastro Removido!",
                    "Seu cadastro foi removido do Spring Boot Microservice!");

            log.info("Removing person {}", id);
            this.personRepository.deleteById(id);

        }else{

            String msg = "Person with id[" + id + "] not found to be removed!";
            log.error(msg);
            throw new EntityNotFoundException(msg);

        }

    };

}
