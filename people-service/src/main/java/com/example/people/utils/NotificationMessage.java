package com.example.people.utils;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class NotificationMessage {

    private String subject;
    private String message;
    private List<String> destinations;

}
