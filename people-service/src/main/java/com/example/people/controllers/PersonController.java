package com.example.people.controllers;

import com.example.people.controllers.exceptions.BadRequestException;
import com.example.people.services.remote.RemoteProjectService;
import com.example.people.utils.ApiResponse;
import com.example.people.entities.Person;
import com.example.people.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Person REST endpoint controllers
 */
@RestController
@RequestMapping(value="/people")
public class PersonController {

    @Autowired
    private PersonService personService;

    @Autowired
    private RemoteProjectService projectService;

    @GetMapping
    public Page<Person> list(Pageable pageable) {
        return this.personService.list(pageable);
    }

    @GetMapping(value="/{id}")
    public Person get(@PathVariable(name="id") Integer id) {
        return this.personService.get(id);
    }

    @GetMapping(value = "/{id}/projects")
    public List<?> getPersonProjects(@PathVariable(name="id") Integer id) {
        ApiResponse<?> apiResponse = this.projectService.getPersonProjects(id);

        if(apiResponse.isSuccess()){
            return (List<?>) apiResponse.getData();
        }

        return Collections.emptyList();

    }

    @PostMapping
    public Person create(@RequestBody Person person){
        return this.personService.register(person);
    }

    @PutMapping(value="/{id}")
    public Person update(@PathVariable(name="id") Integer id, @RequestBody Person person) {

        if(!id.equals(person.getId())) {
            throw new BadRequestException("Person id and path id mismatch!");
        }

        return this.personService.update(person);
    }

    @DeleteMapping(value="/{id}")
    public ApiResponse<?> delete(@PathVariable(name="id") Integer id) {

        this.personService.remove(id);

        return ApiResponse.builder().success(true).data("Person removed!").build();
    }

}